import cv2
import sys
import os
import imutils
import numpy as np

# dnn module
model_file = "models/res10_300x300_ssd_iter_140000_fp16.caffemodel"
config_file = "models/deploy.prototxt"
net = cv2.dnn.readNetFromCaffe(config_file, model_file)
conf_threshold = 0.5

# create tracker
tracker = cv2.TrackerGOTURN_create()

initFace = None
# read capture
cap = cv2.VideoCapture(0)

while True:
    _, frame = cap.read()

    frame = imutils.resize(frame, width=500)
    (h, w) = frame.shape[:2]

    if initFace is not None:
        sucess, initFace = tracker.update(frame)

        # calcullate FPS
        fps = cv2.getTickFrequency() / (cv2.getTickCount() - timer)

        # draw bounding box
        if sucess:
            (x, y, w, h) = [int(v) for v in initFace]
            cX = int((x + x + w) / 2.0)
            cY = int((y + y + h) / 2.0)
            cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
            cv2.circle(frame, (cX, cY), 4, (0, 255, 0), -1)
        else:
            cv2.putText(
                frame,
                "Tracking failure detected",
                (100, 80),
                cv2.FONT_HERSHEY_SIMPLEX,
                0.75,
                (50, 170, 50),
                2,
            )

        # display tracker type on frame
        cv2.putText(
            frame,
            "GOTURN tracker",
            (100, 20),
            cv2.FONT_HERSHEY_SIMPLEX,
            0.75,
            (50, 170, 50),
            2,
        )

        # display FPS on frame
        cv2.putText(
            frame,
            "FPS: " + str(int(fps)),
            (100, 50),
            cv2.FONT_HERSHEY_SIMPLEX,
            0.75,
            (50, 170, 50),
            2,
        )
    else:
        # detect face before tracking
        blob = cv2.dnn.blobFromImage(
            cv2.resize(frame, (300, 300)),
            1.0,
            (300, 300),
            (104.0, 177.0, 123.0),
            swapRB=False,
            crop=False,
        )

        net.setInput(blob)
        detections = net.forward()

        if len(detections) > 0:
            i = np.argmax(detections[0, 0, :, 2])
            confidence = detections[0, 0, i, 2]

            if confidence > conf_threshold:
                box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
                (startX, startY, endX, endY) = box.astype("int")

                # face = frame[startY:endY, startX:endX]
                initFace = (startX, startY, endX - startX, endY - startY)

                tracker.init(frame, initFace)

                # Start timer
                timer = cv2.getTickCount()

    cv2.imshow("Face tracking", frame)
    if cv2.waitKey(1) == 27:
        break

cap.release()
cv2.destroyAllWindows()
