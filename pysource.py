import numpy as np
import time
import cv2

model_file = "models/res10_300x300_ssd_iter_140000_fp16.caffemodel"
config_file = "models/deploy.prototxt"
net = cv2.dnn.readNetFromCaffe(config_file, model_file)

# load the input image and construct an input blob for the image
# by resizing to a fixed 300x300 pixels and then normalizing it

conf_threshold = 0.7

cap = cv2.VideoCapture(0)

frame_count = 0
tt_opencv_dnn = 0

while True:
    _, frame = cap.read()

    frame_count += 1
    t = time.time()

    (h, w) = frame.shape[:2]
    blob = cv2.dnn.blobFromImage(
        cv2.resize(frame, (300, 300)), 1.0, (300, 300), (104.0, 177.0, 123.0)
    )
    # pass the blob through the network and obtain the detection predictions
    net.setInput(blob)
    detections = net.forward()

    for i in range(0, detections.shape[2]):
        # extract the confidence associated with the prediction
        confidence = detections[0, 0, i, 2]

        # filter out weak detections by ensuring the `confidence` is
        # greater than the minimum confidence
        if confidence > conf_threshold:
            # compute the (x, y)-coordinates of the bounding box for the object
            box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
            (startX, startY, endX, endY) = box.astype("int")

            # drawe the bouding box of the face along with the associated probability
            cv2.rectangle(frame, (startX, startY), (endX, endY), (0, 0, 255), 2)

            tt_opencv_dnn += time.time() - t

            fps_opencv_dnn = frame_count / tt_opencv_dnn

            label = "FPS : {:.2f}".format(fps_opencv_dnn)

            text = "{:.2f}%".format(confidence * 100)
            y = startY - 10 if startY - 10 > 10 else startY + 10
            cv2.putText(
                frame, text, (startX, y), cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0, 0, 255), 2
            )
            cv2.putText(
                frame, label, (20, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 255, 0), 2
            )

    cv2.imshow("Face Detection", frame)
    if cv2.waitKey(1) == 27:
        break

cap.release()
cv2.destroyAllWindows()
