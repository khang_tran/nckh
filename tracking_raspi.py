from imutils.video import VideoStream
from imutils.video import FPS
import imutils
import numpy as np
import time
import cv2


class Tracking:
    # dnn module
    model_file = "models/res10_300x300_ssd_iter_140000_fp16.caffemodel"
    config_file = "models/deploy.prototxt"
    net = cv2.dnn.readNetFromCaffe(config_file, model_file)
    conf_threshold = 0.5

    # init frame dimensions
    w = None
    h = None

    # tracking algorithm
    # tracker = cv2.TrackerMOSSE_create()
    tracker = cv2.TrackerKCF_create()

    trackingFace = False

    # init bounding box coordintates of face
    initFace = None

    fps = None

    frame = None

    # def __init__(self, frame):
    #     self.frame = frame

    def processing(self, frame):
        frame = imutils.resize(frame, width=500)
        # if the frame dimensions are empty, set them
        if self.w is None or self.h is None:
            (self.h, self.w) = frame.shape[:2]

        if self.trackingFace == False:
            # detect face before tracking
            blob = cv2.dnn.blobFromImage(
                cv2.resize(frame, (300, 300)),
                1.0,
                (300, 300),
                (104.0, 177.0, 123.0),
                swapRB=False,
                crop=False,
            )

            self.net.setInput(blob)
            detections = self.net.forward()

            if len(detections) > 0:
                i = np.argmax(detections[0, 0, :, 2])
                confidence = detections[0, 0, i, 2]

                if confidence > self.conf_threshold:
                    box = detections[0, 0, i, 3:7] * np.array(
                        [self.w, self.h, self.w, self.h]
                    )
                    (startX, startY, endX, endY) = box.astype("int")

                    # face = frame[startY:endY, startX:endX]
                    self.initFace = (startX, startY, endX - startX, endY - startY)

                    self.tracker.init(frame, self.initFace)

                    self.fps = FPS().start()

                    self.trackingFace = True

        if self.trackingFace == True:
            # grab the new bounding box coordinates of the face
            (success, self.initFace) = self.tracker.update(frame)

            # check to see if the tracking was a sucess
            if success:
                (x, y, w, h) = [int(v) for v in self.initFace]
                cX = int((x + x + w) / 2.0)
                cY = int((y + y + h) / 2.0)
                cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
                cv2.circle(frame, (cX, cY), 4, (0, 255, 0), -1)
            else:
                self.trackingFace = False

            # update the FPS counter
            self.fps.update()
            self.fps.stop()

            # init the set o information
            info = [
                ("Tracking success", "Yes" if success else "No"),
                ("FPS", "{:.2f}".format(self.fps.fps())),
            ]

            # loop over the info tuples and draw them on frame
            for (i, (k, v)) in enumerate(info):
                text = "{}: {}".format(k, v)
                cv2.putText(
                    frame,
                    text,
                    (10, h - ((i * 20) + 20)),
                    cv2.FONT_HERSHEY_SIMPLEX,
                    0.6,
                    (0, 0, 255),
                    2,
                )

        return frame


# test
tracking = Tracking()

vs = VideoStream(src=0).start()

while True:
    frame = vs.read()

    frame = tracking.processing(frame)

    cv2.imshow("Tracking", frame)

    if cv2.waitKey(1) & 0xFF == 27:
        break

vs.stop()
cv2.destroyAllWindows()
