import pigpio
import time
import threading
import cv2

class Servo:
    servoX = 27
    servoY = 17
    angle = 90.0
    serXcurr = 1500
    serYcurr = 1300
    #angle 45 90 135
    #min 900 1500 1900
    def __init__(self):
        self.piX = pigpio.pi()
        self.piX.set_mode(self.servoX, pigpio.OUTPUT)
        self.piX.set_servo_pulsewidth(self.servoX, self.serXcurr)
        self.serXIsRotate = False
        
        self.piY = pigpio.pi()
        self.piY.set_mode(self.servoY, pigpio.OUTPUT)
        self.piY.set_servo_pulsewidth(self.servoY, self.serYcurr)
        self.serYIsRotate = False
        
        # self.p2.start(self.duty(self.angle))
        
    def rotate(self, global_vaX, global_vaY):
        t1 = threading.Thread(target=self.rotateSerX, args=(global_vaX,))
        t2 = threading.Thread(target=self.rotateSerY, args=(global_vaY,))
        t1.start()
        t2.start()
        t2.join()
        
        
    def rotateSerY(self, global_vaY):
        try:
            while True:
                speed = global_vaY.value


                #print(self.piX.get_PWM_frequence(self.servoX))
                if self.serYcurr < 1000 :
                    self.serYcurr = 1000
                    self.serYIsRotate = False
                elif self.serYcurr > 1700:
                    self.serYcurr = 1700
                    self.serYIsRotate = False
                else:
                    self.serYIsRotate = True

                self.serYcurr += (speed/5.0)*2.5

                if self.serYIsRotate:
                    self.piX.set_servo_pulsewidth(self.servoY, self.serYcurr)
                
                time.sleep(0.02)
        except:
            self.piY.stop()
        
    def rotateSerX(self, global_vaX):
        try:
            while True:
                speed = global_vaX.value


                #print(self.piX.get_PWM_frequence(self.servoX))
                if self.serXcurr < 1000 :
                    self.serXcurr = 1000
                    self.serXIsRotate = False
                elif (self.serXcurr > 2000):
                    self.serXcurr = 2000
                    self.serXIsRotate = False
                else:
                    self.serXIsRotate = True

                self.serXcurr -= (speed/5.0)*5.0

                if self.serXIsRotate:
                    self.piX.set_servo_pulsewidth(self.servoX, self.serXcurr)
                
                time.sleep(0.02)
        except:
            self.piX.stop()
            


# if __name__ == "__main__":
#     servo = Servo()
#     servo.update()
  

