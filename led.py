import RPi.GPIO as GPIO
import time


class Led:
	
	led_1 = 22
	GPIO.setmode(GPIO.BCM)
	GPIO.setwarnings(False)
	GPIO.setup(led_1, GPIO.OUT)
	
	led_2 = 4
	GPIO.setmode(GPIO.BCM)
	GPIO.setwarnings(False)
	GPIO.setup(led_2, GPIO.OUT)
	GPIO.output(led_1, GPIO.LOW)
	GPIO.output(led_2, GPIO.LOW)
	
	def remote(self, signal):
		if signal:
			GPIO.output(self.led_1, GPIO.HIGH)
			GPIO.output(self.led_2, GPIO.LOW)
		else:
			GPIO.output(self.led_2, GPIO.HIGH)
			GPIO.output(self.led_1, GPIO.LOW)

	def stop(self):
		GPIO.output(self.led_1, GPIO.LOW)
		GPIO.output(self.led_2, GPIO.LOW)

