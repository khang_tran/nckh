"""
Face tracking using dlib frontal face detector for initialization and dlib discriminative correlation filter tracker
"""

import cv2
import dlib
import numpy as np
import imutils
from imutils.video import VideoStream
from imutils.video import FPS


totalFrames = 0
skip_frames = 60

# dnn module
model_file = "models/res10_300x300_ssd_iter_140000_fp16.caffemodel"
config_file = "models/deploy.prototxt"
net = cv2.dnn.readNetFromCaffe(config_file, model_file)
conf_threshold = 0.5

# init frame dimensions
w = None
h = None


def draw_text_info():
    """Draw text information"""

    # We set the position to be used for drawing text and the menu info:
    menu_pos_1 = (10, 20)
    menu_pos_2 = (10, 40)

    # Write text:
    cv2.putText(
        frame,
        "Use '1' to re-initialize tracking",
        menu_pos_1,
        cv2.FONT_HERSHEY_SIMPLEX,
        0.5,
        (255, 255, 255),
    )
    if tracking_face:
        cv2.putText(
            frame,
            "tracking the face",
            menu_pos_2,
            cv2.FONT_HERSHEY_SIMPLEX,
            0.5,
            (0, 255, 0),
        )
    else:
        cv2.putText(
            frame,
            "detecting a face to initialize tracking...",
            menu_pos_2,
            cv2.FONT_HERSHEY_SIMPLEX,
            0.5,
            (0, 0, 255),
        )


# create the video capture to read from the webcam:
vs = VideoStream(src=0).start()

# load frontal face detector from dlib
detector = dlib.get_frontal_face_detector()

# we initialize the correlation tracker
tracker = dlib.correlation_tracker()

# this variable will hold if we are currently tracking the face:
tracking_face = False

initFace = None

while True:
    # capture frame from webcam
    frame = vs.read()

    frame = imutils.resize(frame, width=500)

    # if the frame dimensions are empty, set them
    if w is None or h is None:
        (h, w) = frame.shape[:2]

    # we draw basic info
    # draw_text_info()

    if tracking_face is False:
        blob = cv2.dnn.blobFromImage(
            cv2.resize(frame, (300, 300)),
            1.0,
            (300, 300),
            (104.0, 177.0, 123.0),
            swapRB=False,
            crop=False,
        )

        net.setInput(blob)
        detections = net.forward()

        if len(detections) > 0:
            i = np.argmax(detections[0, 0, :, 2])
            confidence = detections[0, 0, i, 2]

            if confidence > conf_threshold:
                box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
                (startX, startY, endX, endY) = box.astype("int")

                # face = frame[startY:endY, startX:endX]
                # initFace = (startX, startY, endX - startX, endY - startY)
                initFace = dlib.rectangle(startX - 50, startY, endX + 50, endY + 100)
                # initFace = dlib.rectangle(startX, startY, h, h)

                # start tracking
                tracker.start_track(frame, initFace)

                fps = FPS().start()

                tracking_face = True
            else:
                tracking_face = True

    if tracking_face is True:
        # update tracking and print the peak to side-lobe ratio (measures how confident the tracker is)
        # print(tracker.update(frame))
        tracker.update(frame)

        # update the FPS counter
        fps.update()
        fps.stop()

        # get the position of the tracked object
        pos = tracker.get_position()

        # draw the position
        cv2.rectangle(
            frame,
            (int(pos.left()), int(pos.top())),
            (int(pos.right()), int(pos.bottom())),
            (0, 255, 0),
            3,
        )

        info = "FPS {:.2f}".format(fps.fps())

        cv2.putText(
            frame,
            info,
            (10, h - ((i * 20) + 20)),
            cv2.FONT_HERSHEY_SIMPLEX,
            0.6,
            (0, 0, 255),
            2,
        )

    # we capture the keyboard event
    key = 0xFF & cv2.waitKey(1)

    totalFrames += 1

    if totalFrames % skip_frames == 0:
        # print("Reinit Detection")
        tracking_face = False

    # to exit, press 'q'
    if key == ord("q"):
        break

    # show the resulting image
    cv2.imshow(
        "Face tracking using dlib frontal face detector and correlation filters for tracking",
        frame,
    )

# release everything
vs.stop()
cv2.destroyAllWindows()
