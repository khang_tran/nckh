import cv2
import numpy as np
import imutils
import base64
import zmq
from multiprocessing import Value, Pipe, Process
from final_tracking import Tracking
import time
import imagezmq
import threading
import socket
from imutils.video import VideoStream
from servo import Servo
from led import Led
glo_frame = None
#sender = imagezmq.ImageSender(connect_to='tcp://192.168.0.100:5555')
#rpi_name = socket.gethostname()  # send RPi hostname with each image

def process2(global_vaX,global_vaY):
    control = Servo()
    control.rotate(global_vaX,global_vaY)

def getAndSendFrame():
    global glo_frame

    #context = zmq.Context()
    #footage_socket = context.socket(zmq.PUB)
    #footage_socket.bind('tcp://192.168.0.100:5555')

    rpi_name = socket.gethostname()  # send RPi hostname with each image
    sender = imagezmq.ImageSender(connect_to='tcp://192.168.0.100:5555')
    #cap  = cv2.VideoCapture("/home/pi/2019-09-17-192703.webm")
    cap = cv2.VideoCapture(0)
    while True:
        _, frame = cap.read()
        glo_frame = frame
        #lock.acquire()
        sender.send_image(rpi_name, frame)
        #encoded, buffer = cv2.imencode(".jpg", frame)
        #jpg_as_text = base64.b64encode(buffer)
        #footage_socket.send(jpg_as_text)
        #lock.release()
def process(global_vaX,global_vaY):
    global glo_frame
    #image_hub = imagezmq.ImageHub()

    tracker = Tracking()
    led = Led()
    #sender = imagezmq.ImageSender(connect_to='tcp://192.168.0.100:5555')
    #rpi_name = socket.gethostname()  # send RPi hostname with each image
    t = threading.Thread(target=getAndSendFrame,args=())
    t.start()
    time.sleep(1)
    while True:
        frame = glo_frame
        # print(frame)
        #rpi_name,  = image_hub.recv_image()
        #image_hub.send_reply(b'OK')
        #frame = imutils.resize(frame, width=500)
        if tracker.tracking_face is False:
            tracker.tracking_face = tracker.detection(frame)
            #led.remote(True)

        if tracker.tracking_face is True:
            tracker.tracking_face = tracker.tracking(frame,global_vaX,global_vaY)
            #led.remote(False)

        print(global_vaX.value,global_vaY.value)
        #l.acquire()
        #sender.send_image(rpi_name, frame)
        #l.release()
        cv2.imshow("Tracking", frame)

        key = cv2.waitKey(1) & 0xFF

        if key == 27:
            
            break
    GPIO.cleanup()
    cv2.destroyAllWindows()

global_vaX = Value('f', 0)
global_vaY = Value('f', 0)

p = Process(target=process,args=(global_vaX,global_vaY))
p2 = Process(target=process2,args=(global_vaX,global_vaY))
p.start()
p2.start()
p.join()
p2.join()
print("stoped")
