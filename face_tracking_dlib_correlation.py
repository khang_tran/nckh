import cv2
import dlib

face_cascade = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")

OUTPUT_SIZE_WIDTH = 775
OUTPUT_SIZE_HEIGHT = 600


def detectAndTrackLargestFace():
    capture = cv2.VideoCapture(0)

    # create two opencv named windows
    cv2.namedWindow("base-image", cv2.WINDOW_AUTOSIZE)
    cv2.namedWindow("result-image", cv2.WINDOW_AUTOSIZE)

    # position the windows next to each other
    cv2.moveWindow("base-image", 0, 100)
    cv2.moveWindow("result-image", 400, 100)

    # start the window thread for the two windows we are using
    cv2.startWindowThread()

    # create the tracker we will use
    tracker = dlib.correlation_tracker()

    # the variable we use to keep track of the fact whether we are
    # currently using the dlib tracker
    tracking_face = 0

    # the color of the rectangle we draw around the face
    rectangle_color = (0, 165, 255)

    try:
        while True:
            # retrieve the latest image from the webcam
            rc, full_size_base_image = capture.read()

            # resize the image to 320x240
            base_image = cv2.resize(full_size_base_image, (320, 240))

            pressed_key = cv2.waitKey(1)

            if pressed_key == ord("q"):
                cv2.destroyAllWindows()
                exit(0)

            result_image = base_image.copy()

            # if we are not tracking a face, then try to detect one
            if not tracking_face:
                # convert to gray
                gray = cv2.cvtColor(base_image, cv2.COLOR_BGR2GRAY)

                faces = face_cascade.detectMultiScale(gray, 1.3, 5)

                print("Using the cascade detector to detect face")

                # for now, we are only interested in the 'largest' face,
                # and we determine this based on the largest area of the found rectangle.
                # First initialize the required variables to 0
                max_area = 0
                x = 0
                y = 0
                w = 0
                h = 0

                # loop over all faces and check if the area for this face is the largest so far
                # we need to convert it to int here because of the requirement of the dlib tracker.
                # if we omit the cast to int here, you will get cast errors since
                # the detector returns numpy.int32 and the tracker requires an int
                for (_x, _y, _w, _h) in faces:
                    if _w * _h > max_area:
                        x = int(_x)
                        y = int(_y)
                        w = int(_w)
                        h = int(_h)
                        max_area = w * h

                # if one or more faces are found, initialize the tracker
                # on the largest face in the picture
                if max_area > 0:
                    # initialize the tracker
                    tracker.start_track(
                        base_image,
                        dlib.rectangle(x - 10, y - 20, x + w + 10, y + h + 20),
                    )

                    # set the indicator variable such that we know the
                    # tracker is tracking a region in the image
                    tracking_face = 1

            # check if the tracker is actively tracking a region in the image
            if tracking_face:
                # update the tracker and request information about the quality of the tracking update
                tracking_quality = tracker.update(base_image)

                # if the tracking quality is good enough, determine the updated position
                # of the tracked region and draw the rectangle
                if tracking_quality >= 8.75:
                    tracked_position = tracker.get_position()

                    t_x = int(tracked_position.left())
                    t_y = int(tracked_position.top())
                    t_w = int(tracked_position.width())
                    t_h = int(tracked_position.height())

                    cv2.rectangle(
                        result_image,
                        (t_x, t_y),
                        (t_x + t_w, t_y + t_h),
                        rectangle_color,
                        2,
                    )

                else:
                    # if the quality of te tracking update is not sufficient (e.g. the tracked region moved out
                    # of the screen) we stop the tracking of the face and in the next loop we will find the largest
                    # face in the image again
                    tracking_face = 0

            large_result = cv2.resize(
                result_image, (OUTPUT_SIZE_WIDTH, OUTPUT_SIZE_HEIGHT)
            )

            cv2.imshow("base-image", base_image)
            cv2.imshow("result-image", large_result)

    # to ensure we can also deal with the user pressing Ctrl-C in the console
    # we have to check for the KeyboardInterrupt exception and destroy
    # all opencv windows and exit the application
    except KeyboardInterrupt as e:
        cv2.destroyAllWindows()
        exit(0)


if __name__ == "__main__":
    detectAndTrackLargestFace()
