from imutils.video import VideoStream
from imutils.video import FPS
import imutils
import numpy as np
import time
import cv2

# dnn module
model_file = "models/res10_300x300_ssd_iter_140000_fp16.caffemodel"
config_file = "models/deploy.prototxt"
net = cv2.dnn.readNetFromCaffe(config_file, model_file)
conf_threshold = 0.5

# tracking algorithm
# tracker = cv2.TrackerKCF_create()
tracker = cv2.TrackerKCF_create()
# init bounding box coordintates of face
initFace = None

vs = VideoStream(src=0).start()

fps = None

while True:
    frame = vs.read()

    frame = imutils.resize(frame, width=500)
    (h, w) = frame.shape[:2]

    # check to see if we are currently tracking face
    if initFace is not None:
        # grab the new bounding box coordinates of the face
        (sucess, initFace) = tracker.update(frame)

        # check to see if the tracking was a sucess
        if sucess:
            (x, y, w, h) = [int(v) for v in initFace]
            cX = int((x + x + w) / 2.0)
            cY = int((y + y + h) / 2.0)
            cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
            cv2.circle(frame, (cX, cY), 4, (0, 255, 0), -1)
        else:
            

        # update the FPS counter
        fps.update()
        fps.stop()

        # init the set o information
        info = [
            ("Tracking success", "Yes" if sucess else "No"),
            ("FPS", "{:.2f}".format(fps.fps())),
        ]

        # loop over the info tuples and draw them on frame
        for (i, (k, v)) in enumerate(info):
            text = "{}: {}".format(k, v)
            cv2.putText(
                frame,
                text,
                (10, h - ((i * 20) + 20)),
                cv2.FONT_HERSHEY_SIMPLEX,
                0.6,
                (0, 0, 255),
                2,
            )

    # detect face before tracking
    blob = cv2.dnn.blobFromImage(
        cv2.resize(frame, (300, 300)),
        1.0,
        (300, 300),
        (104.0, 177.0, 123.0),
        swapRB=False,
        crop=False,
    )

    net.setInput(blob)
    detections = net.forward()

    if len(detections) > 0:
        i = np.argmax(detections[0, 0, :, 2])
        confidence = detections[0, 0, i, 2]

        if confidence > conf_threshold:
            box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
            (startX, startY, endX, endY) = box.astype("int")

            # face = frame[startY:endY, startX:endX]
            initFace = (startX, startY, endX - startX, endY - startY)

            tracker.init(frame, initFace)

            fps = FPS().start()

    # Display result
    cv2.imshow("Tracking", frame)

    k = cv2.waitKey(1) & 0xFF
    if k == 27:
        break

cv2.destroyAllWindows()
