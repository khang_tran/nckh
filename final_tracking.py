"""
Face tracking using dlib frontal face detector for initialization and dlib discriminative correlation filter tracker
"""

import cv2
import dlib
import numpy as np
import imutils
from imutils.video import VideoStream
from imutils.video import FPS
import math
from led import Led


class Tracking:
    totalFrames = 0
    led = Led()
    frameCount = 0

    centerX = 0

    # dnn module
    model_file = "models/res10_300x300_ssd_iter_140000_fp16.caffemodel"
    config_file = "models/deploy.prototxt"
    net = cv2.dnn.readNetFromCaffe(config_file, model_file)
    conf_threshold = 0.5

    # init frame dimensions
    w = None
    h = None

    fps = None

    # load frontal face detector from dlib
    detector = dlib.get_frontal_face_detector()

    # we initialize the correlation tracker
    tracker = dlib.correlation_tracker()

    # this variable will hold if we are currently tracking the face:
    tracking_face = False

    initFace = None

    def __init__(self):
        pass

    def detection(self, frame):
        print("Detection")
        # if the frame dimensions are empty, set them
        if self.w is None or self.h is None:
            (self.h, self.w) = frame.shape[:2]

        cv2.putText(
            frame,
            "Detection Face...",
            (self.h - 30, 30),
            cv2.FONT_HERSHEY_SIMPLEX,
            0.6,
            (0, 0, 255),
            2,
        )
        blob = cv2.dnn.blobFromImage(
            cv2.resize(frame, (300, 300)),
            1.0,
            (300, 300),
            (104.0, 177.0, 123.0),
            swapRB=False,
            crop=False,
        )

        self.net.setInput(blob)
        detections = self.net.forward()

        if len(detections) > 0:
            i = np.argmax(detections[0, 0, :, 2])
            confidence = detections[0, 0, i, 2]

            if confidence > self.conf_threshold:
                box = detections[0, 0, i, 3:7] * np.array(
                    [self.w, self.h, self.w, self.h]
                )
                (startX, startY, endX, endY) = box.astype("int")

                # face = frame[startY:endY, startX:endX]
                # initFace = (startX, startY, endX - startX, endY - startY)
                self.initFace = dlib.rectangle(
                    startX - 40, startY, endX + 40, endY + 100
                )
                
                # self.initFace = dlib.rectangle(startX, startY, endX,endY)

                # start tracking
                self.tracker.start_track(frame, self.initFace)

                self.fps = FPS().start()

                return True
            else:
                return False

    # def calAngle(cx, cy, central):
    #     x, y = central

    #     disX = x - cx
    #     angle = math.acos(disX / 250)
    #     angle = math.degrees(angle)
    #     return angle

    def tracking(self, frame, global_vaX, global_vaY):
        # update tracking and print the peak to side-lobe ratio (measures how confident the tracker is)
        # print(tracker.update(frame))
        self.tracker.update(frame)
        #print(frame.shape)
        # update the FPS counter
        self.fps.update()
        self.fps.stop()

        # get the position of the tracked object
        pos = self.tracker.get_position()

        # draw the position
        cv2.rectangle(
            frame,
            (int(pos.left()), int(pos.top())),
            (int(pos.right()), int(pos.bottom())),
            (0, 255, 0),
            3,
        )

        cX = int((pos.left() + pos.right()) / 2.0)
        cY = int((pos.top() + pos.bottom()) / 2.0)

        if cX>420:
            global_vaX.value = 5
        elif cX<220:
            global_vaX.value =-5
        else:
            global_vaX.value=0
            
        if cY > 260:
            global_vaY.value = 5
        elif cY < 140:
            global_vaY.value = -5
        else:
            global_vaY.value = 0

        cv2.circle(frame, (cX, cY), 4, (0, 255, 0), -1)

        info = "FPS {:.2f}".format(self.fps.fps())

        cv2.putText(
            frame, info, (30, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 0, 255), 2
        )

        if self.totalFrames == 0:
            self.centerX = cX

        if self.totalFrames == 10:
            if abs(cX - self.centerX) < 5:
                self.frameCount += 1
                
                # turn red light on
                self.led.stop()
            else:
                self.led.remote(False)
                
            self.centerX = cX
            self.totalFrames = 0
            
    

        if self.frameCount == 8:
            self.frameCount = 0
            self.led.remote(True)
            return False

        self.totalFrames += 1

        return True


# vs = VideoStream(src=0).start()
#
# tracker = Tracking()
#
# while True:
#     frame = vs.read()
#
#     frame = imutils.resize(frame, width=500)
#
#     if tracker.tracking_face is False:
#         tracker.tracking_face = tracker.detection(frame)
#
#     if tracker.tracking_face is True:
#         tracker.tracking_face = tracker.tracking(frame)
#
#     cv2.imshow("Tracking", frame)
#
#     key = cv2.waitKey(1) & 0xFF
#
#     if key == 27:
#         break
#
#     print(tracker.tracking_face)
#
# vs.stop()
# cv2.destroyAllWindows()
