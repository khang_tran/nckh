"""
Face detection using OpenCV DNN face detector
"""

# Import required packages:
import cv2
import numpy as np


# Load pre-trained model:
net = cv2.dnn.readNetFromCaffe(
    "models/deploy.prototxt", "models/res10_300x300_ssd_iter_140000_fp16.caffemodel"
)
# net = cv2.dnn.readNetFromTensorflow("opencv_face_detector_uint8.pb", "opencv_face_detector.pbtxt")

cap = cv2.VideoCapture(0)

conf_threshold = 0.7
while True:
    _, frame = cap.read()

    # Get dimensions of the input image (to be used later):
    (h, w) = frame.shape[:2]

    # Create 4-dimensional blob from image:
    blob = cv2.dnn.blobFromImage(
        frame, 1.0, (300, 300), [104.0, 117.0, 123.0], False, False
    )

    # Set the blob as input and obtain the detections:
    net.setInput(blob)
    detections = net.forward()

    # Iterate over all detections:
    for i in range(0, detections.shape[2]):
        # Get the confidence (probability) of the current detection:
        confidence = detections[0, 0, i, 2]

        # Only consider detections if confidence is greater than a fixed minimum confidence:
        if confidence > conf_threshold:
            # Get the coordinates of the current detection:
            box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
            (startX, startY, endX, endY) = box.astype("int")

            # Draw the detection and the confidence:
            text = "{:.2f}%".format(confidence * 100)
            y = startY - 10 if startY - 10 > 10 else startY + 10
            cv2.rectangle(frame, (startX, startY), (endX, endY), (255, 0, 0), 3)
            cv2.putText(
                frame, text, (startX, y), cv2.FONT_HERSHEY_SIMPLEX, 0.9, (0, 0, 255), 2
            )
            cv2.imshow("Face detection", frame)
    k = cv2.waitKey(1) & 0xFF
    if k == 27:
        break
cap.release()
cv2.destroyAllWindows()

